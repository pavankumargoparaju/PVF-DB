﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using S1G2_PVFAPP.Models;

namespace S1G2_PVFAPP.Controllers
{
    public class ProductsController : Controller
    {
        private Entities6 db = new Entities6();

        // GET: Products
        public ActionResult Index()
        {
            var products = db.Products.Include(p => p.Productline);
            return View(products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            ViewBag.ProductlineID = new SelectList(db.Productlines, "ProductlineID", "LineName");
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,Description,ProductFinish,Price,ProductlineID")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductlineID = new SelectList(db.Productlines, "ProductlineID", "LineName", product.ProductlineID);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductlineID = new SelectList(db.Productlines, "ProductlineID", "LineName", product.ProductlineID);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,Description,ProductFinish,Price,ProductlineID")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductlineID = new SelectList(db.Productlines, "ProductlineID", "LineName", product.ProductlineID);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            List<Production> products = db.Productions.OrderByDescending(p => p.ProductID).ToList();
            for(int i = 0; i < products.Count; i++)
            {
                string text = products[i].ProductID;
                Production productionid = db.Productions.OrderByDescending(q => q.ProductID == text).FirstOrDefault();
                db.Productions.Remove(productionid);
            }


            List<Manufacture> manufactures = db.Manufactures.OrderByDescending(r => r.ProductID).ToList();
            for (int i = 0; i < manufactures.Count; i++)
            {
                string text = manufactures[i].ProductID;
                Manufacture manufactureid = db.Manufactures.OrderByDescending(r => r.ProductID == text).FirstOrDefault();
                db.Manufactures.Remove(manufactureid);
            }

            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
